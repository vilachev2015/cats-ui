import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
/**
 * Класс для реализации логики со страницей рейтинга приложения котиков.
 *
 */
export class RatingPage {
  private page: Page;
//   public buttonSelector: string
  // public rating: string;
  public rating: string;


  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page;
    // this.buttonSelector = '//button[@type="submit"]'
    this.rating = 'table[class="rating-names_table__Jr5Mf"]'
  }


  async openRatingPage() {
    return await test.step('Открываю страницу рейтинга приложения', async () => {
      await this.page.goto('/rating')
      // await this.page.goto('/api/likes/cats/rating')
    })
  }

//   async inputInSearch(data: string) {
//     return await test.step(`Ввожу в строку поиска данные ${data}`, async () => {
//       await this.page.fill('//input[@placeholder]', data);
//     })
//   }
}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
  >;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};

