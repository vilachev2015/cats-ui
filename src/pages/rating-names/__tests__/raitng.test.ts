// import { test } from '@playwright/test';
import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture} from '../__page-object__'


const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

function isNumber(symbol) {
  if ((symbol.charCodeAt(0) >= 48) && (symbol.charCodeAt(0) <= 57)) {
    return true
  }
  else return false
}

function checkRating(_allInTable) {
  let answer = true

  let check = []

  let isTen = true
  let findLikes = true
  let nowLikes = ''
  for (let i = _allInTable.length - 1; i >= 0; i--) {
    if (findLikes) {
      if (isNumber(_allInTable[i])) {
        nowLikes = _allInTable[i] + nowLikes
      }
      else {
        findLikes = false
        check.push(parseInt(nowLikes))
        nowLikes = ''
        if (check.length > 1) {
          if (check[check.length - 1] < check[check.length - 2]) {
            answer = false
            break
          }
        }
      }
    }
    else {
      if (isNumber(_allInTable[i])) {
        if (isTen) {
          if (_allInTable[i].charCodeAt(0) == 49) {
            isTen = false
            findLikes = true
          }
        }
        else {
          if (isNumber(_allInTable[i])) {
            findLikes = true
          }
        }
      }
    }
  }

  return answer
}

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page, ratingPage, }) => {
  /**
   * Замокать ответ метода получения рейтинга ошибкой на стороне сервера
   * Перейти на страницу рейтинга
   * Проверить, что отображается текст ошибка загрузки рейтинга
   */

  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      await route.fulfill({
        status: 500,
      });
    }
  );

  await ratingPage.openRatingPage()


  await expect(page.getByText("Ошибка загрузки рейтинга")).toBeVisible() // Правильный тест
  // await expect(page.getByText("Ошибка загрузки рейтинга")).toBeVisible // Для того, чтоб сломать (домашка DevOps)

  // Файл serverError, вероятно надо будет удалить.
});

test('Рейтинг котиков отображается', async ({ page, ratingPage }) => {
  /**
   * Перейти на страницу рейтинга
   * Проверить, что рейтинг количества лайков отображается по убыванию
   */

  /**
   * Этот тест не всегда проходит проверку верно. Я так понял, ошибка где-то на фронте, так как апи приходит верное.
   * На лекции сказали, мокать ничего не надо.
   */


  await ratingPage.openRatingPage()

  const allInTable = await page.locator(ratingPage.rating).first().textContent();


  let correctRating = checkRating(allInTable)


  // await expect(correctRating).toBeTruthy() // Рабочий тест
  await expect(correctRating).toBeFalsy() // Сломанный тест для домашки DevOPS

});